
import Foundation

struct WeatherFor7days {
    var forecast: [Weather] = []
    var days: [[Weather]] = []
    
    init(json: Dictionary<String, Any>) {
        if let list = json["list"] as? Array<Dictionary<String, Any>> {
            var forcast = [Weather]()
            
            for elem in list {
                forcast.append(Weather(data: elem)!)
            }
            
            days = sortDateByMonth(dateArray: forcast)
            
            for day in days {
                var el: Weather? = nil

                for w in day {
                    if (el == nil) {
                        el = w
                        continue
                    }
                    el!.temp = String(Int(w.temp)! + Int(el!.temp)!)
                }

                el!.temp = String(Int(el!.temp)! / day.count)
                self.forecast.append(el!)
                
            }
        }
    }
    init() {
        
    }
    func sortDateByMonth(dateArray:[Weather]) -> [[Weather]] {
        if dateArray.isEmpty {
            return []
        }
        
        let inputArray = dateArray.sorted()
        var resultArray = [[inputArray[0]]]
        
        let calendar = Calendar(identifier: .gregorian)
        for (prevDate, nextDate) in zip(inputArray, inputArray.dropFirst()) {
            if !calendar.isDate(prevDate.date, equalTo: nextDate.date, toGranularity: .day) {
                resultArray.append([]) // Start new row
            }
            resultArray[resultArray.count - 1].append(nextDate)
        }
        return resultArray
    }
    
    
    
}



