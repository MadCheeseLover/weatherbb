

import UIKit

class WeatherCell2: UITableViewCell {

    @IBOutlet weak var titleLabel2: UILabel!
    
    @IBOutlet weak var detailLabel2: UILabel!
    @IBOutlet weak var imageForTable: UIImageView!
    
}
