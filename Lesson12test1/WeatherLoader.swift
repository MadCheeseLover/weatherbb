
import Foundation

protocol WeatherLoaderDelegate {
    func loadedWeatherCurrent(weatherCurrent: Weather)
    func loadedWeatherFor7Days(forecast: [Weather])
}

class WeatherLoader {
    var delegate: WeatherLoaderDelegate?
    
    
    func getCurrentWeather() {
        guard let res = Persistance.shared.get() else { return }
        self.delegate?.loadedWeatherCurrent(weatherCurrent: res)
       // print(res)
        
        let urlCurrent = URL(string:"http://api.openweathermap.org/data/2.5/weather?q=Moscow&appid=3b70606a950668a8301813a06a587e51")!
        let request = URLRequest(url: urlCurrent)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if let data = data,
                let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? Dictionary<String, Any> {
                do { // Для симуляции долгого http запроса
                    
                    sleep(5)
                }
                
                let weatherCurrent: Weather? = Weather(data: json)
                self.delegate?.loadedWeatherCurrent(weatherCurrent: weatherCurrent!)
                Persistance.shared.insertOrUpdate(weather: weatherCurrent!)
               
                
            }
            
        }
        task.resume()
        
    }
    
    
    func getWeatherFor7Days() {
        guard let res = Persistance.shared.getForForecast() else { return }
        self.delegate?.loadedWeatherFor7Days(forecast: res.forecast)

        
        
        let urlFor7 = URL(string: "http://api.openweathermap.org/data/2.5/forecast?id=524901&APPID=3b70606a950668a8301813a06a587e51")!
        let request = URLRequest(url: urlFor7)
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if let data = data,
                let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? Dictionary<String, Any>
            {
                
         
                let weatherForSevenDays = WeatherFor7days(json: json)
                self.delegate?.loadedWeatherFor7Days(forecast: weatherForSevenDays.forecast)
                Persistance.shared.insertOrUpdateFor7(weatherFor7Days: weatherForSevenDays)
                
            }
            
        }
        task.resume()
    }
    

    
    
}
