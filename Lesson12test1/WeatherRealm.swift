
import Foundation
import RealmSwift

class WeatherRealm: Object {
    @objc dynamic var temp = ""
    @objc dynamic var descriptionRealm = ""
    @objc dynamic var icon = ""
    @objc dynamic var date = Date()
    
}

class WeatherFor7DaysRealm: Object {
  
   var forecast = List<WeatherRealm>()
    

}

class Persistance {
    static let shared = Persistance()
    
    private let realm = try! Realm()
    
    
    func insertOrUpdate(weather: Weather){
        let realm = try! Realm()
        try! realm.write({
            let weatherRealm = WeatherRealm()
            weatherRealm.date = weather.date
            weatherRealm.descriptionRealm = weather.description
            weatherRealm.icon = weather.icon
            weatherRealm.temp = weather.temp
            realm.add(weatherRealm)
            
            //print(weatherRealm)
            
        })
    }
    
    func insertOrUpdateFor7(weatherFor7Days: WeatherFor7days){
        let realm = try! Realm()
        try! realm.write({
            
            let convertForecast = weatherFor7Days.forecast.map { (weather: Weather) -> WeatherRealm in
                    let weatherRealm = WeatherRealm()
                    weatherRealm.date = weather.date
                    weatherRealm.descriptionRealm = weather.description
                    weatherRealm.icon = weather.icon
                    weatherRealm.temp = weather.temp
                    
                    return weatherRealm
                    
            }
            let list: List<WeatherRealm> = List<WeatherRealm>()
            convertForecast.forEach{
                val in
                list.append(val)
            }
            let weatherRealm = WeatherFor7DaysRealm()
            weatherRealm.forecast = list
            realm.add(weatherRealm)
            
            
        })
    }
    
    func get() -> Weather? {
        let realm = try! Realm()
        if let res = realm.objects(WeatherRealm.self).last {
            
            let w = Weather()
            w.date = res.date
            w.description = res.descriptionRealm
            w.temp = res.temp
            w.icon = res.icon
            
            return w
        } else {
            return nil
        }
    }
    
    func getForForecast() -> WeatherFor7days? {
        let realm = try! Realm()
        if let res = realm.objects(WeatherFor7DaysRealm.self).last {
            
            var weatherFor7 = WeatherFor7days()
            
            for realmFmt in Array(res.forecast) {
                let weather = Weather()
                weather.date = realmFmt.date
                weather.description = realmFmt.descriptionRealm
                weather.icon = realmFmt.icon
                weather.temp = realmFmt.temp
                
                weatherFor7.forecast.append(weather)
                
                
            }
            return weatherFor7
        } else {
            return nil
        }
        
    }
}
