

import UIKit

class ViewController2: UIViewController {
    
    @IBOutlet weak var weatherTableView2: UITableView!
    @IBOutlet weak var weatherDescriptionLabel2: UILabel!
    
    @IBOutlet weak var tempLabel2: UILabel!
    
    
    @IBOutlet weak var weatherImage2: UIImageView!
    
   
    var forecast: [Weather] = []
    
    
    var weatherCurrent: Weather = Weather()
    var weatherFor5: WeatherFor7days? = nil
    
    
    func loadedWeatherCurrent(weatherCurrent: Weather) {
        self.tempLabel2.text = weatherCurrent.temp
        self.weatherDescriptionLabel2.text = weatherCurrent.description
        self.weatherImage2.image = UIImage(named: weatherCurrent.icon)
    }
    
    func loadedWeatherFor7Days(forecast: [Weather]) {
        self.forecast = forecast
        self.weatherTableView2.reloadData()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        WeatherLoader2().getCurrentWeather { weatherCurrent in
            self.weatherCurrent = weatherCurrent
            self.loadedWeatherCurrent(weatherCurrent: self.weatherCurrent)
        }
        WeatherLoader2().getWeatherFor7Days { weatherFor7 in
            self.forecast = weatherFor7
            self.loadedWeatherFor7Days(forecast: self.forecast)
            
            
            
        }
    }
}

extension ViewController2: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return forecast.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WeatherCell2", for: indexPath) as! WeatherCell2
        let weather = forecast[indexPath.row]
        cell.titleLabel2.text = "\(weather.getTime())"
        cell.detailLabel2.text = weather.temp
        cell.imageForTable.image = UIImage(named: weatherCurrent.icon)
        
    
        return cell
    }
    
    
    
}
