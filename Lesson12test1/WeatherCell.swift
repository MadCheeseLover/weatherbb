

import UIKit

class WeatherCell: UITableViewCell {

   
    @IBOutlet weak var titleCell: UILabel!
    
    @IBOutlet weak var detailCell: UILabel!
    @IBOutlet weak var imageViewCell: UIImageView!
}
