//
//  ViewController.swift
//  Lesson12test1
//
//  Created by Виктория Щербакова on 08.09.2020.
//  Copyright © 2020 Виктория Щербакова. All rights reserved.
//

import UIKit

import RealmSwift


class ViewController: UIViewController, WeatherLoaderDelegate {
    
    
    @IBOutlet weak var weatherTableView: UITableView!
    
    
    
    
    @IBOutlet weak var background: UIView!
    
    
    @IBOutlet weak var tempLabel: UILabel!
    
    @IBOutlet weak var weatherDescriptionLabel: UILabel!
    
    @IBOutlet weak var weatherImage: UIImageView!
    
    var forecast: [Weather] = []
    
    var weatherCurrent: Weather = Weather()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        

        
        let loader = WeatherLoader()
        loader.delegate = self
        loader.getCurrentWeather()
        loader.getWeatherFor7Days()
        

        
    }
    
    
    

    
    
    
    
    func loadedWeatherCurrent(weatherCurrent: Weather) {
        DispatchQueue.main.async {
            self.weatherCurrent = weatherCurrent
            self.tempLabel.text = weatherCurrent.temp
            self.weatherDescriptionLabel.text = weatherCurrent.description
            self.weatherImage.image = UIImage(named: weatherCurrent.icon)
            
        }
        
    }
    
    
    func loadedWeatherFor7Days(forecast: [Weather]) {
        DispatchQueue.main.async {
            self.forecast = forecast
            
            self.weatherTableView.reloadData()
        }
        
    }
}
extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return forecast.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WeatherCell", for: indexPath) as! WeatherCell
        let weather = forecast[indexPath.row]
        cell.titleCell.text = "\(weather.getTime())"
        cell.detailCell.text = weather.temp
        cell.imageViewCell.image = UIImage(named: self.weatherCurrent.icon)
        print(weatherCurrent.icon)
        return cell
    }
    
    
    
}


