
import Foundation



class Weather: Comparable {
    
    
    
    static func < (lhs: Weather, rhs: Weather) -> Bool {
        return lhs.date < rhs.date
    }
    
    static func == (lhs: Weather, rhs: Weather) -> Bool {
        return lhs.date == rhs.date
    }
    
    var description  = ""
    var temp          = ""
    var icon      = ""
    var date         = Date()
    //var time         = "Error"
    
    init?(data: Dictionary<String, Any>) {
        if let weather = data["weather"] as? Array<Dictionary<String, Any>> {
            self.description = weather.first?["description"] as? String ?? "Error"
            self.description.capitalizeFirstLetter()
            self.icon = weather.first?["icon"] as? String ?? "01d"
            
            
        }
        if let main = data["main"] as? Dictionary<String, Double> {
            let calvin = Int(main["temp"] ?? 0)
            self.temp = (calvin - 273).description
            
        }
        
        if let time = data["dt"] as? Int {
            self.date = Date(timeIntervalSince1970: Double(time))
            //self.time = Weather.getTime(since1970: Double(time))
        }
    }
    
   
    
    init() {
        
    }

    public func getTime() -> String {
        let formatter = DateFormatter()
        formatter.timeZone = .current
        formatter.dateFormat = "EEEE"
        
        
        return formatter.string(from: self.date)
    }
    
    
}
