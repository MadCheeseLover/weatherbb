
import Foundation
import Alamofire

class WeatherLoader2 {
    
    
    
    
    func getCurrentWeather(completion: @escaping (Weather) -> Void) {
        let res = Persistance.shared.get()
      //  completion(res)
       
        
        let request = AF.request("http://api.openweathermap.org/data/2.5/weather?q=Moscow&appid=3b70606a950668a8301813a06a587e51")
        request.responseJSON { response in
            if let objects = response.value,
                let jsonDict = objects as? Dictionary<String, Any> {
                DispatchQueue.main.async {
                    do { // Для симуляции долгого http запроса
                        sleep(5)
                    }
                    let weatherCurrent: Weather? = Weather(data: jsonDict)
                    
                    completion(weatherCurrent!)
                    Persistance.shared.insertOrUpdate(weather: weatherCurrent!)
                    //print(jsonDict)
                }
                
            }
           

        }
    }
    func getWeatherFor7Days(completion: @escaping ([Weather]) -> Void) {
    //    let res = Persistance.shared.getForForecast()
    //    completion(res.forecast)
        let request = AF.request("http://api.openweathermap.org/data/2.5/forecast?id=524901&APPID=3b70606a950668a8301813a06a587e51")
        request.responseJSON { response in
            if let objects = response.value,
                let jsonDict = objects as? Dictionary<String, Any>  {
                DispatchQueue.main.async {
                
                   
                    let weatherForSevenDays = WeatherFor7days(json: jsonDict)
                    completion(weatherForSevenDays.forecast)
                    Persistance.shared.insertOrUpdateFor7(weatherFor7Days: weatherForSevenDays)
                    
                   // print(jsonDict)
                }
            }
        }
    }
    
    
    
        
}
